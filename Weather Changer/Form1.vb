﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        NumericCelsius.Value = ScrollCelsius.Value
        NumericFahren.Value = ScrollFahren.Value
        NumericKelvin.Value = ScrollKelvin.Value

    End Sub

    Private Sub DeScrollAlNumeric(sender As Object, e As EventArgs) Handles ScrollCelsius.ValueChanged, ScrollFahren.ValueChanged, ScrollKelvin.ValueChanged

        NumericCelsius.Value = ScrollCelsius.Value
        NumericFahren.Value = ScrollFahren.Value
        NumericKelvin.Value = ScrollKelvin.Value

    End Sub

    Private Sub DeNumericAScroll(sender As Object, e As EventArgs) Handles NumericCelsius.ValueChanged, NumericFahren.ValueChanged, NumericKelvin.ValueChanged

        ScrollCelsius.Value = NumericCelsius.Value
        ScrollFahren.Value = NumericFahren.Value
        ScrollKelvin.Value = NumericKelvin.Value

    End Sub

    Private Sub convertFharenKelvin(sender As Object, e As EventArgs) Handles ScrollCelsius.ValueChanged

        Try
            ScrollFahren.Value = ((9 / 5) * ScrollCelsius.Value) + 32
            ScrollKelvin.Value = ScrollCelsius.Value + 273

            NumericKelvin.Value = ScrollKelvin.Value
            NumericFahren.Value = ScrollFahren.Value

        Catch ex As System.ArgumentOutOfRangeException

        End Try

    End Sub


    Private Sub convertCelsiusKelvin(sender As Object, e As EventArgs) Handles ScrollFahren.ValueChanged

        Try
            ScrollCelsius.Value = 5 / 9 * (ScrollFahren.Value - 32)
            ScrollKelvin.Value = ScrollCelsius.Value + 273

            NumericCelsius.Value = ScrollCelsius.Value
            NumericKelvin.Value = ScrollKelvin.Value

        Catch ex As System.ArgumentOutOfRangeException

        End Try

    End Sub

    Private Sub convertCelsiusFahren(sender As Object, e As EventArgs) Handles ScrollKelvin.ValueChanged

        Dim kelvinValue = ScrollKelvin.Value

        Try
            ScrollCelsius.Value = kelvinValue - 273
            ScrollFahren.Value = (kelvinValue - 273) * (9 / 5) + 32

            NumericCelsius.Value = ScrollCelsius.Value
            NumericKelvin.Value = ScrollKelvin.Value

        Catch ex As System.ArgumentOutOfRangeException

        End Try

    End Sub


End Class

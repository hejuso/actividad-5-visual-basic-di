﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ScrollCelsius = New System.Windows.Forms.TrackBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.NumericCelsius = New System.Windows.Forms.NumericUpDown()
        Me.ScrollFahren = New System.Windows.Forms.TrackBar()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.NumericFahren = New System.Windows.Forms.NumericUpDown()
        Me.ScrollKelvin = New System.Windows.Forms.TrackBar()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.NumericKelvin = New System.Windows.Forms.NumericUpDown()
        CType(Me.ScrollCelsius, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericCelsius, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ScrollFahren, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericFahren, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ScrollKelvin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericKelvin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ScrollCelsius
        '
        Me.ScrollCelsius.Location = New System.Drawing.Point(85, 73)
        Me.ScrollCelsius.Maximum = 100
        Me.ScrollCelsius.Minimum = -273
        Me.ScrollCelsius.Name = "ScrollCelsius"
        Me.ScrollCelsius.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.ScrollCelsius.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ScrollCelsius.RightToLeftLayout = True
        Me.ScrollCelsius.Size = New System.Drawing.Size(45, 374)
        Me.ScrollCelsius.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 36)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Celsius"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(216, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(141, 36)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Fahrenheit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(409, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 36)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Kelvin"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(127, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(25, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "100"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(127, 173)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(127, 431)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(28, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "-273"
        '
        'NumericCelsius
        '
        Me.NumericCelsius.Location = New System.Drawing.Point(67, 466)
        Me.NumericCelsius.Minimum = New Decimal(New Integer() {273, 0, 0, -2147483648})
        Me.NumericCelsius.Name = "NumericCelsius"
        Me.NumericCelsius.Size = New System.Drawing.Size(81, 20)
        Me.NumericCelsius.TabIndex = 3
        '
        'ScrollFahren
        '
        Me.ScrollFahren.Location = New System.Drawing.Point(257, 73)
        Me.ScrollFahren.Maximum = 212
        Me.ScrollFahren.Minimum = -460
        Me.ScrollFahren.Name = "ScrollFahren"
        Me.ScrollFahren.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.ScrollFahren.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ScrollFahren.RightToLeftLayout = True
        Me.ScrollFahren.Size = New System.Drawing.Size(45, 374)
        Me.ScrollFahren.TabIndex = 0
        Me.ScrollFahren.Value = 32
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(299, 80)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(25, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "212"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(299, 173)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(19, 13)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "32"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(299, 431)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(28, 13)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "-460"
        '
        'NumericFahren
        '
        Me.NumericFahren.Location = New System.Drawing.Point(239, 466)
        Me.NumericFahren.Maximum = New Decimal(New Integer() {212, 0, 0, 0})
        Me.NumericFahren.Minimum = New Decimal(New Integer() {460, 0, 0, -2147483648})
        Me.NumericFahren.Name = "NumericFahren"
        Me.NumericFahren.Size = New System.Drawing.Size(81, 20)
        Me.NumericFahren.TabIndex = 3
        '
        'ScrollKelvin
        '
        Me.ScrollKelvin.Location = New System.Drawing.Point(433, 73)
        Me.ScrollKelvin.Maximum = 373
        Me.ScrollKelvin.Name = "ScrollKelvin"
        Me.ScrollKelvin.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.ScrollKelvin.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ScrollKelvin.RightToLeftLayout = True
        Me.ScrollKelvin.Size = New System.Drawing.Size(45, 374)
        Me.ScrollKelvin.TabIndex = 0
        Me.ScrollKelvin.Value = 273
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(475, 80)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(25, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "373"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(475, 173)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(25, 13)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "273"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(475, 431)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(13, 13)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "0"
        '
        'NumericKelvin
        '
        Me.NumericKelvin.Location = New System.Drawing.Point(415, 466)
        Me.NumericKelvin.Maximum = New Decimal(New Integer() {373, 0, 0, 0})
        Me.NumericKelvin.Name = "NumericKelvin"
        Me.NumericKelvin.Size = New System.Drawing.Size(81, 20)
        Me.NumericKelvin.TabIndex = 3
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 548)
        Me.Controls.Add(Me.NumericKelvin)
        Me.Controls.Add(Me.NumericFahren)
        Me.Controls.Add(Me.NumericCelsius)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ScrollKelvin)
        Me.Controls.Add(Me.ScrollFahren)
        Me.Controls.Add(Me.ScrollCelsius)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.Text = "Weather changer"
        CType(Me.ScrollCelsius, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericCelsius, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ScrollFahren, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericFahren, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ScrollKelvin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericKelvin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ScrollCelsius As TrackBar
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents NumericCelsius As NumericUpDown
    Friend WithEvents ScrollFahren As TrackBar
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents NumericFahren As NumericUpDown
    Friend WithEvents ScrollKelvin As TrackBar
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents NumericKelvin As NumericUpDown
End Class
